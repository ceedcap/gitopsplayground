create k8s cluster
install helm
create local argo-cd chart
#create namespaces
helm repo add argo-cd https://argoproj.github.io/argo-helm
helm repo add jetstack https://charts.jetstack.io
helm dep update charts/argocd/

echo "charts/" > charts/argocd/.gitignore

git add charts/argo-cd
git commit -m 'add argocd chart'
git push

#install & configure argocd
helm install argocd charts/argocd/ -n argocd --create-namespace
kubectl get pods -l app.kubernetes.io/name=argocd-server -o name -n argocd | cut -d'/' -f 2
kubectl port-forward svc/argocd-server 8080:443 -n argocd


#sealed secrets
wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.12.4/kubeseal-linux-amd64 -O kubeseal
sudo install -m 755 kubeseal /usr/local/bin/kubeseal

#helm repo add sealed-secrets https://bitnami-labs.github.io/sealed-secrets
#helm install --namespace kube-system helm-sealed-secrets sealed-secrets/sealed-secrets

kubeseal --fetch-cert --controller-name=sealed-secrets --controller-namespace=kube-system > pub-cert.pem

#kubectl create secret generic bitbucket -n argo-cd -o yaml --dry-run=client --from-file=ssh-privatekey=/Users/osas/.ssh/id_rsa --type=kubernetes.io/ssh-auth >secret.yaml
kubectl create secret generic bitbucket -n argocd -o yaml --dry-run=client --from-literal=username=fintis --from-literal=password=XXX --type=kubernetes.io/basic-auth >secret.yaml
kubeseal --cert pub-cert.pem --format yaml <secret.yaml >apps/staging/templates/repo-secret.yaml

git add apps
git ci -m 'add root app'
git push

helm template apps/staging | kubectl apply -f -

#delete bootstrapped argo from helm
kubectl delete secret -l owner=helm,name=argocd

kafka
postgre/mariadb
mongodb
cassandra
keycloak/hydra
istio
open policy
jaeger
longhorn
elasticsearch
spark
zendesk
bitbucket
bamboo



